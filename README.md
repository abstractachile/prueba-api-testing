# Ejercicio práctico: API Testing


La tienda de mascotas “PerfDog” se encuentra desarrollando una nueva versión para su sitio web. 

Dado que el front end no está terminado se decide comenzar a probar las funcionalidades utilizando la API de la aplicación.

La tienda nos provee la documentación de la API para que podamos crear las pruebas: https://petstore.swagger.io

Las funcionalidades que se debe probar son las siguientes:

```
1. Crear un usuario.
2. Hacer login con el usuario recién creado.
3. Listar todas las mascotas que tengan el status “disponible”.
4. Consultar los datos de una mascota en específico (cualquiera).
5. Crear una orden (compra) para una mascota.
6. Hacer el logout a la aplicación.
```

Se pide:

```
- Crear una colección de Postman que contenga las invocaciones necesarias para completar las funcionalidades.
- Cada invocación debe tener al menos un test.
- Todos los test deben ser distintos por cada invocación.
- Crear al menos 3 variables dentro de la colección. (Tipo: variable de colección).
```

Entregar en un archivo zip o en un repositorio de GitLab o GitHub (público) la colección y cualquier otro archivo necesario para probar la misma.
